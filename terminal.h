#include <Arduino.h> //For String
#include "string.h"
#define MAX_CHARS 32
class Terminal {
    public:
        Terminal();
        char screen_buffer[MAX_CHARS];
        int curr_index;
        void del_prev(void);
        void set_curr_char(char c);
        void clear(void);
        String get_buffer(void);
};