#include "terminal.h"

Terminal::Terminal()
{
    memset(screen_buffer,0,sizeof(screen_buffer));
    curr_index = 0;
};

String Terminal::get_buffer() {
    return String(screen_buffer);
};

void Terminal::del_prev(void) {
    memset(&screen_buffer[curr_index],'\0', sizeof(char));
    curr_index--;
};

void Terminal::clear(void) {
    memset(screen_buffer,0,sizeof(screen_buffer));
    curr_index = 0;
};

void Terminal::set_curr_char(char c)
{
    screen_buffer[curr_index++] = c;
}