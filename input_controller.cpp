#include <Arduino.h> //For String
#include "input_controller.h"

InputController::InputController(void)
{
    char start_char = 'a';
    curr_index = 0;
    
    for (int i = 0; i < MAX_INPUT_CHAR; i++)
    {
        all_chars[i] = start_char++;
    }
    all_chars[MAX_INPUT_CHAR-1]='_';
}
char InputController::get_curr_selection()
{
    return all_chars[curr_index];
}

void InputController::inc_index(int val)
{
    curr_index = (((curr_index + val) % MAX_INPUT_CHAR) + MAX_INPUT_CHAR) % MAX_INPUT_CHAR;

    Serial.println("Current indx:" + String(curr_index));
}

void InputController::dec_index(int val)
{
    curr_index = (((curr_index - val) % MAX_INPUT_CHAR) + MAX_INPUT_CHAR) % MAX_INPUT_CHAR;

    Serial.println("Current indx:" + String(curr_index));
}

void InputController::reset_index(void)
{
    curr_index = 0;
}