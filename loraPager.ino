#include <ClickEncoder.h>
#include <TimerOne.h>
#include "input_controller.h"
#include "terminal.h"
#include "oled_display.h"

int16_t oldEncPos, encPos=1;
uint8_t buttonState;

#define pinA 7
#define pinB 8
#define pinSw 9 //switch
#define STEPS 4

ClickEncoder encoder(pinA, pinB, pinSw, STEPS);

//U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NO_ACK| U8G_I2C_OPT_FAST);

Terminal term;
InputController inputController;
OledDisplay disp;

void setup()
{

  Serial.begin(9600);

  Serial.println("START.....");
  Timer1.initialize(500);
  Timer1.attachInterrupt(timerIsr);

  encoder.setAccelerationEnabled(true);

  Serial.print("Acceleration is ");
  Serial.println((encoder.getAccelerationEnabled()) ? "enabled" : "disabled");

  oldEncPos = 1;
}

void loop()
{
  //Serial.println(".");
  encPos += encoder.getValue();

  //Create blink effect
  

  if (encPos != oldEncPos)
  {
    int diff = encPos - oldEncPos;
    inputController.dec_index(diff);
    oldEncPos = encPos;    
    //disp.add_char(encPos, false);
    Serial.print("Encoder Value: ");
    Serial.println(encPos);
    Serial.println(String("Char :")+String(inputController.get_curr_selection()));
  }

  disp.add_char_with_blink_effect(inputController.get_curr_selection(), false);

  buttonState = encoder.getButton();

  if (buttonState != 0)
  {
    Serial.print("Button: ");
    Serial.println(buttonState);
    switch (buttonState)
    {
    case ClickEncoder::Open: //0
      break;

    case ClickEncoder::Closed: //1
      break;

    case ClickEncoder::Pressed: //2
      break;

    case ClickEncoder::Held: //3
      break;

    case ClickEncoder::Released: //4
      break;

    case ClickEncoder::Clicked: //5
    {
      term.set_curr_char(inputController.get_curr_selection());
      disp.add_char_with_blink_effect(inputController.get_curr_selection(), true);
      inputController.reset_index();
      Serial.println(term.get_buffer());
      break;
    }

    case ClickEncoder::DoubleClicked: //6
      break;
    }
  }
}

void timerIsr()
{
  encoder.service();
}
