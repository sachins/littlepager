#define MAX_INPUT_CHAR 30
class InputController
{
  public:
    InputController(void);
    unsigned char all_chars[MAX_INPUT_CHAR];
    int curr_index;
    char get_curr_selection();
    void inc_index(int val);
    void dec_index(int val);
    void reset_index();
};
