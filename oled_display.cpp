#include "oled_display.h"
//U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NO_ACK| U8G_I2C_OPT_FAST);

// Arduino master setup
OledDisplay::OledDisplay()
{

  char_displayed_when_blinking = ' ';
  MAX_BLINK_TIME = 300;
  last_blink_time = 0;

  u8g = new U8GLIB_SSD1306_128X64(U8G_I2C_OPT_NO_ACK | U8G_I2C_OPT_FAST);

  // set font for the console window
  //u8g->setFont(u8g_font_5x8);
  u8g->setFont(u8g_font_9x15);

  // set upper left position for the string draw procedure
  u8g->setFontPosTop();

  // calculate the number of rows for the OledDisplay
  rows = u8g->getHeight() / u8g->getFontLineSpacing();
  if (rows > ROW_MAX)
    rows = ROW_MAX;

  // estimate the number of columns for the OledDisplay
  cols = u8g->getWidth() / u8g->getStrWidth("m");
  if (cols > LINE_MAX - 1)
    cols = LINE_MAX - 1;

  clear_display_buffer(); // clear screen

  exec_line(false); // place the input buffer into the screen
  reset_line();     // clear input buffer

  
}

// clear entire screen, called during setup
void OledDisplay::clear_display_buffer(void)
{

  uint8_t i, j;
  for (i = 0; i < ROW_MAX; i++)
    for (j = 0; j < LINE_MAX; j++)
      screen[i][j] = 0;
}

// append a line to the screen, scroll up
// if is_new_line is FALSE, then just refresh old line with new data
void OledDisplay::add_line_to_screen(boolean is_new_line)
{
  uint8_t i, j;
  if (is_new_line)
  {
    //clear_display_buffer();
    //Shift lines up except for last
    for (j = 0; j < LINE_MAX; j++)
      for (i = 0; i < rows - 1; i++)
        screen[i][j] = screen[i + 1][j];

    //Clear last line
    for (j = 0; j < LINE_MAX; j++)
      screen[rows - 1][j] = '\0';
  }
  else
  {
    for (j = 0; j < LINE_MAX; j++)
      screen[rows - 1][j] = line_buf[j];
  }
}

// U8GLIB draw procedure: output the screen
void OledDisplay::draw(void)
{
  uint8_t i, y;
  // graphic commands to redraw the complete screen are placed here
  y = 0; // reference is the top left -1 position of the string
  y--;   // correct the -1 position of the drawStr
  for (i = 0; i < rows; i++)
  {
    u8g->drawStr(0, y, (char *)(screen[i]));
    y += u8g->getFontLineSpacing();
  }
}

void OledDisplay::exec_line(boolean is_new_line)
{
  // echo line to the serial monitor
  //Serial.println((const char *)line_buf);

  // add the line to the screen
  add_line_to_screen(is_new_line);

  // U8GLIB picture loop
  u8g->firstPage();
  do
  {
    draw();
  } while (u8g->nextPage());
}

// clear current input buffer
void OledDisplay::reset_line(void)
{
  line_pos = 0;
  line_buf[line_pos] = '\0';
}

// add a single character to the input buffer
void OledDisplay::char_to_line(uint8_t c)
{
  line_buf[line_pos] = c;
  line_pos++;
  line_buf[line_pos] = '\0';
}

void OledDisplay::add_char_with_blink_effect(char c, boolean move_cursor)
{

  if (!move_cursor)
  {
    blinking_char = c;
    //If OledDisplay on same spot, fix line_pos here so later code does not need change
    line_pos--;
    if (blink)
    {
      c = char_displayed_when_blinking;
    }
    if (millis() - last_blink_time > MAX_BLINK_TIME)
    {
      blink = !blink;
      last_blink_time = millis();
    }
  }
  else
  {
    //if cursor moved when blinking, put the last blinking char in the previous place instead of char_displayed_when_blinking
    line_buf[line_pos - 1] = blinking_char;
  }
  if (line_pos >= cols - 1)
  {
    exec_line(true);
    reset_line();
    char_to_line(c);
  }
  else if (c == '\r' || c == '\n')
  {
    exec_line(true);
    reset_line();
  }
  else
  {
    //Serial.println("in else");
    char_to_line(c);
    //Serial.println("buf:"+String(line_buf));
    add_line_to_screen(false);
    exec_line(false);
  }
}
