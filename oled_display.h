#include "U8glib.h"
#include <Arduino.h> //For delay

// setup input buffer
#define LINE_MAX 30
// setup a text screen to support scrolling
#define ROW_MAX 12
// line height, which matches the selected font (5x7)
#define LINE_PIXEL_HEIGHT 7

class OledDisplay
{
    U8GLIB_SSD1306_128X64 *u8g;
    unsigned char line_buf[LINE_MAX];
    uint8_t line_pos = 0;
    char screen[ROW_MAX][LINE_MAX];
    uint8_t rows, cols;
    //Char which is currently highlighted and blinking
    char blinking_char;
    //Char to be displayed when current highlighted char is blinked off
    char char_displayed_when_blinking;
    boolean blink;
    unsigned long MAX_BLINK_TIME;
    unsigned long last_blink_time;

  public:
    OledDisplay();

    void clear_display_buffer();
    void add_line_to_screen(boolean is_new_line);
    void draw();
    void exec_line(boolean is_new_line);
    void reset_line();
    void char_to_line(uint8_t c);
    void refresh();
    void add_char_with_blink_effect(char c, boolean move_cursor);
};
